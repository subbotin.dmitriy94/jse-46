package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IUserDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.IUserDtoService;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.dto.UserDtoRepository;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoService extends AbstractDtoService<UserDTO> implements IUserDtoService {

    public UserDtoService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<UserDTO> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public UserDTO findById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public UserDTO findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findById(id);
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findByLogin(login);
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findByIndex(realIndex);
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            user.setRole(role);
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final String email
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(login);
            user.setPasswordHash(getPasswordHash(password));
            user.setRole(role);
            user.setEmail(email);
            entityManager.getTransaction().begin();
            userRepository.create(user);
            entityManager.getTransaction().commit();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<UserDTO> users) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            for (UserDTO user : users) userRepository.create(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public UserDTO findByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String password
    ) throws AbstractException {
        checkId(id);
        checkPassword(password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final String passwordHash = getPasswordHash(password);
            @NotNull final UserDTO user = userRepository.findById(id);
            user.setPasswordHash(passwordHash);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setRole(
            @NotNull final String id,
            @NotNull final Role role
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findById(id);
            user.setRole(role);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findById(id);
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLogin(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            checkLogin(login);
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.findByLogin(login);
            return true;
        } catch (AbstractException e) {
            return false;
        }
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findByLogin(login);
            user.setLocked(true);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final UserDTO user = userRepository.findByLogin(login);
            user.setLocked(false);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void checkLogin(@NotNull final String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(@NotNull final String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(iteration, secret, password);
    }

}