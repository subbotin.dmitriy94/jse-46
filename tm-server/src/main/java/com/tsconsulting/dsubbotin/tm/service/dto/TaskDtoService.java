package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.ITaskDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.ITaskDtoService;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.repository.dto.TaskDtoRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class TaskDtoService extends AbstractOwnerDtoService<TaskDTO> implements ITaskDtoService {

    public TaskDtoService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @Override
    public @NotNull List<TaskDTO> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAll();
            if (tasks.isEmpty()) throw new TaskNotFoundException();
            return tasks;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<TaskDTO> findAll(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAll(userId);
            if (tasks.isEmpty()) throw new TaskNotFoundException();
            return tasks;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final String sort) throws AbstractException {
        @NotNull List<TaskDTO> tasks = findAll(userId);
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            tasks.sort(sortType.getComparator());
            return tasks;
        } catch (UnknownSortException e) {
            return tasks;
        }
    }

    @NotNull
    @Override
    public TaskDTO findById(@NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findByIndex(realIndex);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findByIndex(userId, realIndex);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName(name);
            task.setDescription(description);
            task.setUserId(userId);
            entityManager.getTransaction().begin();
            taskRepository.create(userId, task);
            entityManager.getTransaction().commit();
            return taskRepository.findById(task.getId());
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<TaskDTO> tasks) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : tasks) taskRepository.create(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findById(userId, id);
            entityManager.getTransaction().begin();
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByIndex(userId, realIndex);
            entityManager.getTransaction().begin();
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByName(userId, name);
            entityManager.getTransaction().begin();
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findById(userId, id);
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByIndex(userId, realIndex);
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findById(userId, id);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByIndex(userId, realIndex);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkId(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findById(userId, id);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByIndex(@NotNull final String userId, final int index) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByIndex(userId, realIndex);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findById(userId, id);
            task.setStatus(status);
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        final int realIndex = index - 1;
        checkIndex(realIndex);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = taskRepository.findByIndex(userId, realIndex);
            task.setStatus(status);
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(status);
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}