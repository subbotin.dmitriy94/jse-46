package com.tsconsulting.dsubbotin.tm.dto;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "users")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractEntityDTO {

    @Column
    boolean locked = false;

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Override
    @NotNull
    public String toString() {
        return super.toString() + " : " + login + "; Role: " + role.getDisplayName() + ";";
    }

}
