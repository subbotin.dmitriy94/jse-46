package com.tsconsulting.dsubbotin.tm.api.service.dto;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionDtoService extends IDtoService<SessionDTO> {

    @NotNull
    SessionDTO open(
            @NotNull String login,
            @NotNull String password
    ) throws AbstractException;

    boolean close(@NotNull SessionDTO session);

    void validate(@Nullable SessionDTO session) throws AbstractException;

    void validate(@Nullable SessionDTO session, @NotNull Role role) throws AbstractException;

    @NotNull
    UserDTO getUser(@NotNull SessionDTO session) throws AbstractException;

    @NotNull
    String getUserId(@NotNull SessionDTO session);

}
