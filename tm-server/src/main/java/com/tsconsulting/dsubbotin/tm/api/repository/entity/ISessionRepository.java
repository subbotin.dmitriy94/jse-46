package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.Session;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void open(@NotNull Session session);

    @NotNull
    List<Session> findAll(@NotNull String userId);

    void close(@NotNull Session session);

    boolean contains(@NotNull Session session);

    boolean existByUserId(@NotNull String userId);

    boolean existById(@NotNull String id);

    void clear(@NotNull String userId);

}
