package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @NotNull
    UserDTO findByLogin(@NotNull String login) throws AbstractException;

    void clear(@NotNull String userId);

}
