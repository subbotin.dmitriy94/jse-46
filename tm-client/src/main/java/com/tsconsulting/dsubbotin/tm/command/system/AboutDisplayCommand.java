package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class AboutDisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        TerminalUtil.printMessage("Developer: " + endpointLocator.getPropertyService().getDeveloperName());
        TerminalUtil.printMessage("E-Mail: " + endpointLocator.getPropertyService().getDeveloperEmail());
    }

}
