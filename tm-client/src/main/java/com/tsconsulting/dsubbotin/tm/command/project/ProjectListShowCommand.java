package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<ProjectDTO> projects = endpointLocator.getProjectEndpoint().findAllProject(session, sort);
        int index = 1;
        for (@NotNull final ProjectDTO project : projects)
            TerminalUtil.printMessage(index++ + "." + showProjectLine(project));
    }

}
